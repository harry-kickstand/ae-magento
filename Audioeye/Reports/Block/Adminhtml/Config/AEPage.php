<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Block\Adminhtml\Config;

use Audioeye\Reports\Helper\Data;

class AEPage extends \Magento\Backend\Block\Template
{

    /**
     * Block template.
     *
     * @var string
     */
    protected $_template = 'page/aepage.phtml';

    /**
     * @var \Audioeye\Reports\Helper\Data
     */
    protected $_helper;

    /**
     * @var string
     */
    protected $_signature;

    /**
     * @var string
     */
    protected $_timestamp;

    /**
     * AEPage constructor.
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param \Audioeye\Reports\Helper\Data               $helper
     * @param array                                    $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Audioeye\Reports\Helper\Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->_signature = "";
        $this->_timestamp = "";

        if ($helper->isAccountInstalled())
        {
            $response = $helper->requestAccountDetail();

            if (isset($response['signature']) && $response['signature']) {
                $this->_signature = $response["signature"];
            }

            if (isset($response['timestamp']) && $response['timestamp']) {
                $this->_timestamp = $response["timestamp"];
            }
        }

        parent::__construct($context, $data);
    }

    /**
     * isAccountInstalled
     *
     * @return bool
     */
    public function isAccountInstalled()
    {
        return $this->_helper->isAccountInstalled();
    }

    /**
     * getSignature
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->_signature;
    }

    /**
     * getTimestamp
     *
     * @return string
     */
    public function getTimestamp()
    {
        return $this->_timestamp;
    }

    /**
     * getIframeSrc
     *
     * @return string
     */
    public function getIframeSrc()
    {
        $result = sprintf(
            '%s?ext_id=%s&sig=%s&ts=%s&channel_name=magento&redirect=reports',
            Data::IFRAME_SRC,
            $this->_helper->getExtId(),
            $this->getSignature(),
            $this->getTimestamp()
        );
        return $result;
    }
}
