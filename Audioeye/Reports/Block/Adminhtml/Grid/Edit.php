<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Block\Adminhtml\Grid;

/**
 * Block for edit page
 */

class Edit extends \Magento\Backend\Block\Widget\Form\Container {

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $_redirect;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_redirect = $redirect;
        parent::__construct($context, $data);
    }

    /**
     * Init container
     *
     * @return void
     */
    protected function _construct() {
        $this->_objectId = 'id';
        $this->_blockGroup = 'audioeye_reports';
        $this->_controller = 'adminhtml_grid';

        parent::_construct();
    }

    /**
     * Get edit form container header text
     *
     * @return \Magento\Framework\Phrase|string
     */
    public function getHeaderText() {
        return __('Setting');
    }

    /**
     * Get form save URL
     *
     * @see getFormActionUrl()
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('*/*/saveSetting', ['_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}']);
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->_redirect->getRefererUrl();
    }

    /**
     * Prepare the layout.
     *
     * @return $this
     */
    protected function _prepareLayout() {
        return parent::_prepareLayout();
    }

}
