<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Block\Adminhtml\Grid\Edit;

/**
 * Admin left menu
 *
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs {

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('grid_record');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('General'));
    }
}
