<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Block\Frontend;

use Audioeye\Reports\Helper\Data;

class DynamicScript extends \Magento\Backend\Block\Template
{

    /**
     * Block template.
     *
     * @var string
     */
    protected $_template = 'aescript.phtml';

    /**
     * @var \Audioeye\Reports\Helper\Data
     */
    protected $_helper;

    /**
     * AEPage constructor.
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param \Audioeye\Reports\Helper\Data            $helper
     * @param array                                    $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Audioeye\Reports\Helper\Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;

        parent::__construct($context, $data);
    }

    /**
     * isAccountInstalled
     *
     * @return bool
     */
    public function isAccountInstalled()
    {
        return $this->_helper->isAccountInstalled();
    }

    /**
     * getSiteHash
     *
     * @return string
     */
    public function getSiteHash()
    {
        return $this->_helper->getSiteHash();
    }
}
