<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Helper;

use Audioeye\Reports\Model\ResourceModel\Config\CollectionFactory as ConfigCollectionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\HTTP\Client\Curl;

/**
 * Class Data
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Email Config Data
     */
    const EMAIL_CONFIG_DATA = 'email';

    /**
     * BaseUrl Config Data
     */
    const BASEURL_CONFIG_DATA = 'base_url';

    /**
     * Account Unique ID Config Data
     */
    const EXTID_CONFIG_DATA = 'ext_id';

    /**
     * Site Hash Config Data
     */
    const SITEHASH_CONFIG_DATA = 'site_hash';

    /**
     * Install Account POST URL
     */
    const INSTALL_URL = 'http://cms-server-ae-ks.herokuapp.com/magento/install';

    /**
     * Account Detail GET URL
     */
    const DETAIL_URL = 'http://cms-server-ae-ks.herokuapp.com/magento/signature/?ext_id=';

    /**
     * Iframe src
     */
    const IFRAME_SRC = 'https://portalstaging2.audioeye.com/login/channel';

    /**
     * ConfigCollectionFactory
     */
    private $_configCollectionFactory;

    /**
     * @var Curl
     */
    private $_curl;

    /**
     * Constructor
     *
     * @param Context $context
     * @param ConfigCollectionFactory $configCollectionFactory
     * @param Curl $curl
     */
    public function __construct(
        Context $context,
        ConfigCollectionFactory $configCollectionFactory,
        Curl $curl
    ) {
        $this->_configCollectionFactory = $configCollectionFactory;
        $this->_curl = $curl;

        parent::__construct($context);
    }

    /**
     * getEmail
     *
     * @return string
     */
    public function getEmail()
    {
        $collection = $this->_configCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                'config_data',
                ['eq' => self::EMAIL_CONFIG_DATA]
            );
        $data = $collection->getData();

        if (count($data) > 0) {
            return $data[0]['config_value'];
        }

        return '';
    }

    /**
     * getEmailId
     *
     * @return int|null
     */
    public function getEmailId()
    {
        $collection = $this->_configCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                'config_data',
                ['eq' => self::EMAIL_CONFIG_DATA]
            );
        $data = $collection->getData();

        if (count($data) > 0) {
            return $data[0]['config_id'];
        }

        return null;
    }

    /**
     * getBaseUrl
     *
     * @return string
     */
    public function getBaseUrl()
    {
        $collection = $this->_configCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                'config_data',
                ['eq' => self::BASEURL_CONFIG_DATA]
            );
        $data = $collection->getData();

        if (count($data) > 0) {
            return $data[0]['config_value'];
        }

        return '';
    }

    /**
     * getBaseUrlId
     *
     * @return int|null
     */
    public function getBaseUrlId()
    {
        $collection = $this->_configCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                'config_data',
                ['eq' => self::BASEURL_CONFIG_DATA]
            );
        $data = $collection->getData();

        if (count($data) > 0) {
            return $data[0]['config_id'];
        }

        return null;
    }

    /**
     * getExtId
     *
     * @return string
     */
    public function getExtId()
    {
        $collection = $this->_configCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                'config_data',
                ['eq' => self::EXTID_CONFIG_DATA]
            );
        $data = $collection->getData();

        if (count($data) > 0) {
            return $data[0]['config_value'];
        }

        return '';
    }

    /**
     * getExtIdId
     *
     * @return int|null
     */
    public function getExtIdId()
    {
        $collection = $this->_configCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                'config_data',
                ['eq' => self::EXTID_CONFIG_DATA]
            );
        $data = $collection->getData();

        if (count($data) > 0) {
            return $data[0]['config_id'];
        }

        return null;
    }

    /**
     * getSiteHash
     *
     * @return string
     */
    public function getSiteHash()
    {
        $collection = $this->_configCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                'config_data',
                ['eq' => self::SITEHASH_CONFIG_DATA]
            );
        $data = $collection->getData();

        if (count($data) > 0) {
            return $data[0]['config_value'];
        }

        return '';
    }

    /**
     * getSiteHashId
     *
     * @return int|null
     */
    public function getSiteHashId()
    {
        $collection = $this->_configCollectionFactory->create();
        $collection->addFieldToSelect('*')
            ->addFieldToFilter(
                'config_data',
                ['eq' => self::SITEHASH_CONFIG_DATA]
            );
        $data = $collection->getData();

        if (count($data) > 0) {
            return $data[0]['config_id'];
        }

        return null;
    }

    /**
     * isConfigAvailable
     *
     * @return bool
     */
    public function isConfigAvailable()
    {
        $result = ($this->getEmail() != '') && ($this->getBaseUrl() != '');
        return $result;
    }

    /**
     * isAccountInstalled
     *
     * @return bool
     */
    public function isAccountInstalled()
    {
        $result = ($this->getExtId() != '');
        return $result;
    }

    /**
     * getConfigData
     *
     * @return array
     */
    public function getConfigData()
    {
        $result = ["email" => $this->getEmail(), "base_url" => $this->getBaseUrl()];
        return $result;
    }

    /**
     * requestAccountInstall
     *
     * @return mixed
     */
    public function requestAccountInstall()
    {
        $params = $this->getConfigData();
        $url = self::INSTALL_URL;
        $this->_curl->post($url, $params);
        $response = $this->_curl->getBody();
        return json_decode($response, true);
        //string(188) "{"ext_id":"cfb80b11138c616e8f38b5d7d406db15b15221dd5dfbe0cfb4625484784a5772de54181f5685bed258907edc30abd165a31c748e3f8e3fe770f2b4ffcb3920f0","site_hash":"bd20eb5b352e83f08c3fda97c7c6c56c"}"
        //string(188) "{"ext_id":"ca62e26b9dbff2a4c61a2f1c27e6678889653f7041a1001f85978c0ac554c30800ef6dc0e7c9b07e36c1b55d9d3c99300334b69170e138e0c8515ac004eed939","site_hash":"1aa033d9fdae644dc88725ebe97b0813"}"
    }

    /**
     * requestAccountDetail
     *
     * @return mixed
     */
    public function requestAccountDetail()
    {
        $url = self::DETAIL_URL . $this->getExtId();
        $this->_curl->get($url);
        $response = $this->_curl->getBody();
        return json_decode($response, true);
    }
}
