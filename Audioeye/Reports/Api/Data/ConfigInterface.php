<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Api\Data;

/**
 * Interface ConfigInterface
 *
 * @package Audioeye\Reports\Api\Data
 */
interface ConfigInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CONFIG_VALUE = 'config_value';
    const UPDATED_AT = 'updated_at';
    const CONFIG_ID = 'config_id';
    const CONFIG_DATA = 'config_data';
    const CREATED_AT = 'created_at';

    /**
     * Get config_id
     * @return string|null
     */
    public function getConfigId();

    /**
     * Set config_id
     * @param string $configId
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setConfigId($configId);

    /**
     * Get config_data
     * @return string|null
     */
    public function getConfigData();

    /**
     * Set config_data
     * @param string $configData
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setConfigData($configData);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Audioeye\Reports\Api\Data\ConfigExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Audioeye\Reports\Api\Data\ConfigExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Audioeye\Reports\Api\Data\ConfigExtensionInterface $extensionAttributes
    );

    /**
     * Get config_value
     * @return string|null
     */
    public function getConfigValue();

    /**
     * Set config_value
     * @param string $configValue
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setConfigValue($configValue);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setUpdatedAt($updatedAt);
}

