<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Api\Data;

/**
 * Interface ConfigSearchResultsInterface
 *
 * @package Audioeye\Reports\Api\Data
 */
interface ConfigSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get config list.
     * @return \Audioeye\Reports\Api\Data\ConfigInterface[]
     */
    public function getItems();

    /**
     * Set config_data list.
     * @param \Audioeye\Reports\Api\Data\ConfigInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

