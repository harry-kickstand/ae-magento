<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ConfigRepositoryInterface
 *
 * @package Audioeye\Reports\Api
 */
interface ConfigRepositoryInterface
{

    /**
     * Save config
     * @param \Audioeye\Reports\Api\Data\ConfigInterface $config
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Audioeye\Reports\Api\Data\ConfigInterface $config
    );

    /**
     * Retrieve config
     * @param string $configId
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($configId);

    /**
     * Retrieve config matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Audioeye\Reports\Api\Data\ConfigSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete config
     * @param \Audioeye\Reports\Api\Data\ConfigInterface $config
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Audioeye\Reports\Api\Data\ConfigInterface $config
    );

    /**
     * Delete config by ID
     * @param string $configId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($configId);
}

