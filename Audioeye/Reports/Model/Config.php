<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Model;

use Magento\Framework\Api\DataObjectHelper;
use Audioeye\Reports\Api\Data\ConfigInterface;
use Audioeye\Reports\Api\Data\ConfigInterfaceFactory;

/**
 * Class Config
 *
 * @package Audioeye\Reports\Model
 */
class Config extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'audioeye_reports_config';
    protected $configDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ConfigInterfaceFactory $configDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Audioeye\Reports\Model\ResourceModel\Config $resource
     * @param \Audioeye\Reports\Model\ResourceModel\Config\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ConfigInterfaceFactory $configDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Audioeye\Reports\Model\ResourceModel\Config $resource,
        \Audioeye\Reports\Model\ResourceModel\Config\Collection $resourceCollection,
        array $data = []
    ) {
        $this->configDataFactory = $configDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve config model with config data
     * @return ConfigInterface
     */
    public function getDataModel()
    {
        $configData = $this->getData();

        $configDataObject = $this->configDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $configDataObject,
            $configData,
            ConfigInterface::class
        );

        return $configDataObject;
    }
}

