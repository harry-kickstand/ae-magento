<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Model\Data;

use Audioeye\Reports\Api\Data\ConfigInterface;

/**
 * Class Config
 *
 * @package Audioeye_Reports
 */
class Config extends \Magento\Framework\Api\AbstractExtensibleObject implements ConfigInterface
{

    /**
     * Get config_id
     * @return string|null
     */
    public function getConfigId()
    {
        return $this->_get(self::CONFIG_ID);
    }

    /**
     * Set config_id
     * @param string $configId
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setConfigId($configId)
    {
        return $this->setData(self::CONFIG_ID, $configId);
    }

    /**
     * Get config_data
     * @return string|null
     */
    public function getConfigData()
    {
        return $this->_get(self::CONFIG_DATA);
    }

    /**
     * Set config_data
     * @param string $configData
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setConfigData($configData)
    {
        return $this->setData(self::CONFIG_DATA, $configData);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Audioeye\Reports\Api\Data\ConfigExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Audioeye\Reports\Api\Data\ConfigExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Audioeye\Reports\Api\Data\ConfigExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get config_value
     * @return string|null
     */
    public function getConfigValue()
    {
        return $this->_get(self::CONFIG_VALUE);
    }

    /**
     * Set config_value
     * @param string $configValue
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setConfigValue($configValue)
    {
        return $this->setData(self::CONFIG_VALUE, $configValue);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \Audioeye\Reports\Api\Data\ConfigInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}

