<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Audioeye\Reports\Model\ResourceModel\Config\CollectionFactory as ConfigCollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Audioeye\Reports\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Audioeye\Reports\Api\ConfigRepositoryInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Audioeye\Reports\Api\Data\ConfigSearchResultsInterfaceFactory;
use Audioeye\Reports\Api\Data\ConfigInterfaceFactory;

/**
 * Class ConfigRepository
 *
 * @package Audioeye\Reports\Model
 */
class ConfigRepository implements ConfigRepositoryInterface
{

    protected $dataConfigFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $configCollectionFactory;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $resource;

    private $storeManager;

    protected $extensibleDataObjectConverter;
    protected $configFactory;


    /**
     * @param ResourceConfig $resource
     * @param ConfigFactory $configFactory
     * @param ConfigInterfaceFactory $dataConfigFactory
     * @param ConfigCollectionFactory $configCollectionFactory
     * @param ConfigSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceConfig $resource,
        ConfigFactory $configFactory,
        ConfigInterfaceFactory $dataConfigFactory,
        ConfigCollectionFactory $configCollectionFactory,
        ConfigSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->configFactory = $configFactory;
        $this->configCollectionFactory = $configCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataConfigFactory = $dataConfigFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Audioeye\Reports\Api\Data\ConfigInterface $config
    ) {
        /* if (empty($config->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $config->setStoreId($storeId);
        } */

        $configData = $this->extensibleDataObjectConverter->toNestedArray(
            $config,
            [],
            \Audioeye\Reports\Api\Data\ConfigInterface::class
        );

        $configModel = $this->configFactory->create()->setData($configData);

        try {
            $this->resource->save($configModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the config: %1',
                $exception->getMessage()
            ));
        }
        return $configModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($configId)
    {
        $config = $this->configFactory->create();
        $this->resource->load($config, $configId);
        if (!$config->getId()) {
            throw new NoSuchEntityException(__('config with id "%1" does not exist.', $configId));
        }
        return $config->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->configCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Audioeye\Reports\Api\Data\ConfigInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Audioeye\Reports\Api\Data\ConfigInterface $config
    ) {
        try {
            $configModel = $this->configFactory->create();
            $this->resource->load($configModel, $config->getConfigId());
            $this->resource->delete($configModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the config: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($configId)
    {
        return $this->delete($this->get($configId));
    }
}

