<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Model\ResourceModel\Config;

/**
 * Class Collection
 *
 * @package Audioeye_Reports
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'config_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Audioeye\Reports\Model\Config::class,
            \Audioeye\Reports\Model\ResourceModel\Config::class
        );
    }
}

