<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Model\System\Message;

use Magento\Framework\Notification\MessageInterface;
use Magento\Framework\UrlInterface;
use Audioeye\Reports\Helper\Data;

/**
 * Class CustomSystemMessage
 */
class CustomSystemMessage implements MessageInterface
{
    /**
     * Message identity
     */
    const MESSAGE_IDENTITY = 'audioeye_system_message';

    /**
     * Configuration URL
     */
    const URL_PATH_CONFIG = 'audioeye_reports/config/setting';

    /**
     * UrlInterface
     */
    private $_urlBuilder;

    /**
     * Data
     */
    private $_data;

    /**
     * Constructor
     *
     * @param UrlInterface $urlBuilder
     * @param Data $data
     */
    public function __construct(
        UrlInterface $urlBuilder,
        Data $data
    ) {
        $this->_urlBuilder = $urlBuilder;
        $this->_data = $data;
    }

    /**
     * Retrieve unique system message identity
     *
     * @return string
     */
    public function getIdentity()
    {
        return self::MESSAGE_IDENTITY;
    }

    /**
     * Check whether the system message should be shown (true: show, false: hide)
     *
     * @return bool
     */
    public function isDisplayed()
    {
        return !$this->_data->isConfigAvailable();
    }

    /**
     * Retrieve system message text
     *
     * @return string
     */
    public function getText()
    {
        $result = sprintf(
            '%s <a class="action" href="%s">%s</a> %s',
            __('AudioEye configuration is missing. Click link '),
            $this->_urlBuilder->getUrl(self::URL_PATH_CONFIG),
            __('here'),
            __('to complete the configuration.')
        );
        return $result;
    }

    /**
     * Retrieve system message severity
     * Possible default system message types:
     * - MessageInterface::SEVERITY_CRITICAL
     * - MessageInterface::SEVERITY_MAJOR
     * - MessageInterface::SEVERITY_MINOR
     * - MessageInterface::SEVERITY_NOTICE
     *
     * @return int
     */
    public function getSeverity()
    {
        return self::SEVERITY_MAJOR;
    }
}
