<?php
/**
 * @author Petar Todorovski <petartodorovskigod@gmail.com>
 * @copyright Copyright (c) 2020 Kickstand
 * @package Audioeye_Reports
 */

namespace Audioeye\Reports\Controller\Adminhtml\Config;

use Magento\Framework\Exception\LocalizedException;
use Audioeye\Reports\Helper\Data;

/**
 * Class SaveSetting
 *
 * @package Audioeye_Reports
 */
class SaveSetting extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param Data $helperData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        Data $helperData
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->helperData = $helperData;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data && isset($data['email']) && isset($data['base_url'])) {
            $emailId = $this->helperData->getEmailId();
            $modelEmailConfig = $this->_objectManager->create(\Audioeye\Reports\Model\Config::class)->load($emailId);
            $modelEmailConfig->setConfigData(Data::EMAIL_CONFIG_DATA);
            $modelEmailConfig->setConfigValue($data['email']);

            $baseUrlId = $this->helperData->getBaseUrlId();
            $modelBaseUrlConfig = $this->_objectManager->create(\Audioeye\Reports\Model\Config::class)->load($baseUrlId);
            $modelBaseUrlConfig->setConfigData(Data::BASEURL_CONFIG_DATA);
            $modelBaseUrlConfig->setConfigValue($data['base_url']);

            try {
                $modelEmailConfig->save();
                $modelBaseUrlConfig->save();

                $responseInstall = $this->helperData->requestAccountInstall();

                /** Save ext_id into db. */
                if (isset($responseInstall['ext_id']) && $responseInstall['ext_id']) {
                    $extId = $this->helperData->getExtIdId();
                    $modelExtConfig = $this->_objectManager->create(\Audioeye\Reports\Model\Config::class)->load($extId);
                    $modelExtConfig->setConfigData(Data::EXTID_CONFIG_DATA);
                    $modelExtConfig->setConfigValue($responseInstall['ext_id']);
                    $modelExtConfig->save();
                }

                /** Save site_hash into db. */
                if (isset($responseInstall['site_hash']) && $responseInstall['site_hash']) {
                    $siteHashId = $this->helperData->getSiteHashId();
                    $modelSiteHashConfig = $this->_objectManager->create(\Audioeye\Reports\Model\Config::class)->load($siteHashId);
                    $modelSiteHashConfig->setConfigData(Data::SITEHASH_CONFIG_DATA);
                    $modelSiteHashConfig->setConfigValue($responseInstall['site_hash']);
                    $modelSiteHashConfig->save();
                }

                $this->messageManager->addSuccessMessage(__('You saved successfully.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving. Please try again later!'));
            }
        }
        return $resultRedirect->setPath($this->_redirect->getRefererUrl());
    }
}

